<h1 align="center">
  <br>
  <a href="metapay.vn"><img src="./public/images/logo-colored.png" alt="metapay" width="100"></a>
</h1>
<p align="center">
    <img src='https://forthebadge.com/images/badges/made-with-javascript.svg' />
    <img src='https://forthebadge.com/images/badges/uses-css.svg' />
    <img src='https://forthebadge.com/images/badges/validated-html5.svg' />
    <img src='https://forthebadge.com/images/badges/uses-html.svg' />
</p>
<h4 align="center">Metapay FE with React</h4>


[![React](https://img.shields.io/badge/react-16.2.0-lightgrey.svg)](https://github.com/facebook/react)
[![Jest](https://img.shields.io/badge/Jest-22.4.3-lightgrey.svg)](https://jestjs.io/)

- This repo holds the entire front end code base for Metapay.The code is written in React 16.
- This repo was bootstrapped with CRA(CREATE-REACT-APP) and has been ejected.
- For styling we are using bootstrap and antd
- Test cases are written in Jest and snapshot tests in Enzyme

## 📦 Table of Contents

1.  [Requirements](#requirements)
2.  [Installation](#getting-started)
3.  [Running the Project](#running-the-project)
4.  [Project Structure](#project-structure)
5.  [Routing](#routing)
6.  [Development Tools](#development-tools)
7.  [Building for Production](#building-for-production)
9.  [Deployment](#deployment)

## 💼 Requirements
How to set-up FE environment http://confluencevn.koreacentral.cloudapp.azure.com:8090/display/MPAY/Setup+FE+environment

- node `^12.13.0`
- yarn `^1.19.0`

## 💾 Installation

After confirming that your environment meets the above [requirements](#requirements), you can start this project by following the steps mentioned below:-

```bash
$ git clone metapay-frontend
$ cd metapay-frontend
```

When that's done, install the project dependencies. It is recommended that you use [Yarn](https://yarnpkg.com/) for deterministic dependency management, but `npm install` will suffice.

```bash
$ yarn install # Install project dependencies (or `npm install`)

```


## ▶️ Running the Project

After completing the [installation](#installation) step, you're ready to start the project!
When you are running this project for the first time, you need to follow these steps:-

Since the project relies on a lot of environment variables, one needs to create a copy of the properties_sample.env file inside config folder and save as properties.env
```bash
# For development environment

$ cp env/properties.sample.env env/properties.env # Make a properties.env file from properties.sample.env

```
Make changes in it according to the environment variables you need, we use [dotenv](https://www.npmjs.com/package/dotenv) which will read all environment variables from properties.env and set them on process.env


### For react project execution

```bash
# For development environment

$ yarn start # Build the client bundles and start the webpack dev server (or `npm run start`)

```

While developing, you will probably rely mostly on `yarn start`; however, there are additional scripts at your disposal:

|`yarn <script>`                                |Description|
|-----------------------------------------------|-----------|
|`yarn start`                                   |Starts the app at `localhost:3000` by default|
|`yarn build`                                   |Builds the app in production mode and serves files from build folder|
|`yarn test`                                    |Runs all your jest and enzyme test's|

## ✏️ Project Structure

We are using postcss to apply css modular and React context to manage App state.

```
├── build                                       # All production ready files with minified JS, html and css files
├── config                                      # All CRA related config goes here including paths, environment variables and │jest config goes here
├── ci                                          # CI config goes here
├── public                                      # Static public assets used while in dev mode
├── scripts                                     # All webpack related code
│   ├── build.js                                # Script for making production bundle
│   ├── start.js                                # Script for development mode
│   ├── test.js                                 # Script for test mode
├── src                                         # Client Application source code
│   ├── __mocks__                               # All global mocks for jest testing goes here
│   ├── __mock_data__                           # All mock data goes here.
│   ├── helpers                                 # All api helpers, utils, local storage, analytics and config helpers go inside this folder
│   ├── apis                                    # All api endpoints and api related logic goes here
│   │   ├── index.js                              # The api calling logic resides here
│   ├── components                              # Global Reusable Components
│   │   ├── ComponentName                       # Component Name Folder and every component will have a index.js and css file
│   │   │   ├── index.js                        # Main component code
│   │   │   ├── index.module.css                # Styling for the component
│   ├── containers                              # Global Reusable Components
│   │   ├── ContainerName                       # Pages Folder and every page will have a index.js and css file
│   │   │   ├── index.js                        # Main file which exports the page
│   │   │   ├── reducer.js                      # Redux Reducer of this container
│   │   │   ├── index.module.css                # Styling for the page
│   ├── index.js                                # Application bootstrap and rendering
│   ├── constants                               # Folder for constants/config files 
│   ├── App.js                                  # All application client side routes using react-router
├── env                                         # All environment variables to be configured from here
│   ├── properties.sample.env                   # Sample file for setting up environment vars
├── .babelrc                                    # Babel file for es6 and react code transpilation
├── .gitignore                                  # The name says it all
├── .eslintrc.js                                # This file maintains all end points of the back end routes
├── .prettierrc                                 # Prettier config
├── package.json                                # All npm dependencies can be found here
├── README.md                                   # Readme file for the whole app
├── yarn.lock                                   # Yarn lock file for locking the dependency versions
```

## 🚀 Routing

We use `react-router` [route definitions](https://github.com/ReactTraining/react-router)
See the [project structure](#project-structure) section for more information.

## ⚙️ Development Tools

### Prettier

- We use `prettier` for code formatting.Here is the link to downlod the same.[Prettier](https://www.npmjs.com/package/prettier)

- Make sure you are using vscode and your vscode user_settings has the following code:-

```bash
{
    "editor.fontSize": 12,
    "editor.formatOnPaste": true,
    "editor.formatOnSave": true,
    "prettier.eslintIntegration": true,
}
```

## 🚚 Building for Production

## Deployment

- Deployment will always happen from the `production` branch on production.
- Any production related environment variables need to be configured on env/properties.env.Take a copy of sample and edit values for prod environment
- yarn build will built the production build
