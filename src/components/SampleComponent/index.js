import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './index.module.css';

class SampleComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={classNames('row justify-content-center', styles.wrapper)}>
        <div className={classNames('col-12', styles.wrapper)}>
          <div className='text-center text-white'>
            <h2>Metapay</h2>
            <p>
              this is a boideplate of React + Redux + Saga
            </p>
          </div>
        </div>
      </div>
    );
  }
}

SampleComponent.propTypes = {
  userInfo: PropTypes.object,
};

SampleComponent.defaultProps = {
  userInfo: {}
};

export default SampleComponent;