import React from 'react';
import PropTypes from 'prop-types';
import className from 'classnames';

import Header from 'components/Header';
import Footer from 'components/Footer';

import styles from './mainLayout.module.css';

function MainLayout({ location, children }) {
  return (
    <div className={className('container-fluid', styles.wrapper)}>
      <Header location={location} />
      { children }
      <Footer />
    </div>
  );
}

MainLayout.propTypes = {
  children: PropTypes.object, 
  location: PropTypes.object,
};

export default MainLayout;
