import React from 'react';

import styles from './footer.module.css';

export default () => {
  return (
    <div className={styles.wrapper}>Footer</div>
  );
};