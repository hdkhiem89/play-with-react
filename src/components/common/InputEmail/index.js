import React from 'react';

import styles from './index.module.css';

export default props => {
  return <input className={styles.wrapper} name='email' type='email' {...props} />;
};