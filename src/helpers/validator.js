var isNull = require('lodash/isNil');
var isEmpty = require('lodash/isEmpty');
var isString = require('lodash/isString');
var isInteger = require('lodash/isInteger');
var filter = require('lodash/filter');
var moment = require('moment');
var phoneRegex = /^(0|84)(9\d|16[2-9]|12\d|86|88|89|186|188|199)(\d{7})$/;
var phoneRegexNewPrefix = /^(0|84)(70|79|77|76|78|83|84|85|81|82|32|33|34|35|36|37|38|39|52|56|58|59)(\d{7})$/;
var phoneRegexIllegalPrefix = /^(0|84)(12[0-9]|16[2-9]|186|188|199)(\d{7})$/;
var nationalIdRegex = /^\d{9}(\d{3})?$/;
var imageExtRegex = /.*(jpg|jpeg|png|tif|gif)/;
var accentMarksRegex = /à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|ì|í|ị|ỉ|ĩ|ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|ỳ|ý|ỵ|ỷ|ỹ|đ/i;
var provinceCodeNew = {'Hà Nội': '001','Hà Giang': '002','Cao Bằng': '004','Bắc Kạn': '006','Tuyên Quang': '008','Lào Cai': '010','Điện Biên': '011','Lai Châu': '012','Sơn La': '014','Yên Bái': '015','Hòa Bình': '017','Thái Nguyên': '019','Lạng Sơn': '020','Quảng Ninh': '022','Bắc Giang': '024','Phú Thọ': '025','Vĩnh Phúc': '026','Bắc Ninh': '027','Hải Dương': '030','Hải Phòng': '031','Hưng Yên': '033','Thái Bình': '034','Hà Nam': '035','Nam Định': '036','Ninh Bình': '037','Thanh Hóa': '038','Nghệ An': '040','Hà Tĩnh': '042','Quảng Bình': '044','Quảng Trị': '045','Thừa Thiên Huế': '046','Đà Nẵng': '048','Quảng Nam': '049','Quảng Ngãi': '051','Bình Định': '052','Phú Yên': '054','Khánh Hòa': '056','Ninh Thuận': '058','Bình Thuận': '060','Kon Tum': '062','Gia Lai': '064','Đắk Lắk': '066','Đắk Nông': '067','Lâm Đồng': '068','Bình Phước': '070','Tây Ninh': '072','Bình Dương': '074','Đồng Nai': '075','Bà Rịa - Vũng Tàu': '077','TP HCM': '079','Long An': '080','Tiền Giang': '082','Bến Tre': '083','Trà Vinh': '084','Vĩnh Long': '086','Đồng Tháp': '087','An Giang': '089','Kiên Giang': '091','Cần Thơ': '092','Hậu Giang': '093','Sóc Trăng': '094','Bạc Liêu': '095','Cà Mau': '096',};
var provinceCodeOld = [['An Giang', '35'],['Bắc Giang', '12'],['Bắc Kạn', '095'],['Bạc Liêu', '38'],['Bắc Ninh', '12'],['Bà Rịa - Vũng Tàu', '27'],['Bến Tre', '32'],['Bình Định', '21'],['Bình Dương', '280'],['Bình Dương', '281'],['Bình Phước', '285'],['Bình Thuận', '26'],['Cà Mau', '38'],['Cần Thơ', '36'],['Cao Bằng', '08'],['Đắk Lắk', '24'],['Đắk Nông', '245'],['Đà Nẵng', '20'],['Điện Biên', '04'],['Đồng Nai', '27'],['Đồng Tháp', '34'],['Gia Lai', '230'],['Gia Lai', '231'],['Hà Giang', '07'],['Hải Dương', '14'],['Hải Phòng', '03'],['Hà Nam', '16'],['Hà Nội', '01'],['Hà Nội', '11'],['Hà Tĩnh', '18'],['Hậu Giang', '36'],['Hòa Bình', '11'],['Hưng Yên', '14'],['Khánh Hòa', '22'],['Kiên Giang', '37'],['Kon Tum', '23'],['Lai Châu', '04'],['Lâm Đồng', '25'],['Lạng Sơn', '08'],['Lào Cai', '06'],['Long An', '30'],['Nam Định', '16'],['Nghệ An', '18'],['Ninh Bình', '16'],['Ninh Thuận', '26'],['Phú Thọ', '13'],['Phú Yên', '22'],['Quảng Bình', '19'],['Quảng Nam', '20'],['Quảng Ngãi', '21'],['Quảng Ninh', '10'],['Quảng Trị', '19'],['Sóc Trăng', '36'],['Sơn La', '05'],['Tây Ninh', '29'],['Thái Bình', '15'],['Thái Nguyên', '090'],['Thái Nguyên', '091'],['Thái Nguyên', '092'],['Thanh Hóa', '17'],['Thừa Thiên Huế', '19'],['Tiền Giang', '31'],['TP HCM', '02'],['Trà Vinh', '33'],['Tuyên Quang', '07'],['Vĩnh Long', '33'],['Vĩnh Phúc', '13'],['Yên Bái', '15'],['Yên Bái', '06']];

export function isMobilePhoneNumber(p) {
  if (!isString(p)) return false;
  return phoneRegex.test(p) || phoneRegexNewPrefix.test(p);
}

export function isValidPrefixPhoneNumber(p) {
  if (!isString(p)) return false;
  return !phoneRegexIllegalPrefix.test(p);
}

export function isNationalIdNumber(p) {
  if (!isString(p)) return false;
  return nationalIdRegex.test(p);
}

export function isMonthlyIncome(p) {
  if (!isInteger(p)) return false;
  return p >= 0;
}

export function isValidImage(p) {
  if (!isString(p)) return false;
  return imageExtRegex.test(p);
}

export function isValidImageSize(p, maxSizeInMB=10) {
  if (!isInteger(p)) return false;
  return p <= maxSizeInMB * 1048576;
}

export function hasAccentMarks(p){
  if (!isString(p)) return false;
  return accentMarksRegex.test(p);
}

export function validateNewNationId(nid, provinceValue, genderStr, dob) {
  if (!(isString(nid) && isString(provinceValue) && isString(genderStr) && dob)) return false;

  const assertedProvinceCode = nid.slice(0,3);
  const assertedGender = nid.slice(3,4);
  const assertedYearOfBirth = nid.slice(4,6);
  const yearOfBirth = moment(dob).get('year');

  const isValidProvinceCode = provinceCodeNew[provinceValue] && assertedProvinceCode === provinceCodeNew[provinceValue] ? true : false;
  if (!isValidProvinceCode) return false;

  // Male: 0 and Female: 1
  let gender = genderStr === 'male' ? 0 : 1;
  // after 2000, the gender will be Male: 2 and Female: 3
  if (yearOfBirth >= 2000) {
    gender += 2;
  }
  let isValidGender = gender.toString() === assertedGender;
  if (!isValidGender) return false;

  let isValidYearOfBirth = yearOfBirth.toString().slice(2) === assertedYearOfBirth;
  if (!isValidYearOfBirth) return false;

  return true;
}

export function validateOldNationId(nid, provinceValue) {
  const provinces = filter(provinceCodeOld, function(o) { return o[0] === provinceValue;});
  if (provinces && provinces.length > 0)  {
    return provinces.some(function(p) {
      const provinceCode = p[1];
      const assertedProvinceCode = nid.slice(0, provinceCode.length);
      return assertedProvinceCode === provinceCode;
    });
  }

  return false;
}

export function isValidNationalIdNumber(nid, provinceValue, gender, yearOfBirth) {
  if (!isNationalIdNumber(nid)) return false;
    
  if (nid.length === 12) {
    return validateNewNationId(nid, provinceValue, gender, yearOfBirth);
  } else {
    return validateOldNationId(nid, provinceValue);
  }
}
