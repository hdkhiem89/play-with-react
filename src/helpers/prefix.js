import isString from 'lodash/isString';
const phoneRegexNewPrefix = /^(0|84)(70|79|77|76|78|83|84|85|81|82|32|33|34|35|36|37|38|39|52|56|58|59)(\d{7})$/;
const phoneRegexIllegalPrefix = /^(0|84)(12[0-9]|16[2-9]|186|188|199)(\d{7})$/;
const config = {
  validator: {
    new_prefix_convert: {
      mobifone: {
        '0120': '070',
        '0121': '079',
        '0122': '077',
        '0126': '076',
        '0128': '078',
      },
      vinaphone: {
        '0123': '083',
        '0124': '084',
        '0125': '085',
        '0127': '081',
        '0129': '082',
      },
      viettel: {
        '0162': '032',
        '0163': '033',
        '0164': '034',
        '0165': '035',
        '0166': '036',
        '0167': '037',
        '0168': '038',
        '0169': '039',
      },
      vietnamobile: {
        '0186': '056',
        '0188': '058',
      },
      gmobile: {
        '0199': '059',
      },
    },
    telco: {
      viettel: /^(0|84)(9[6-8]|16[2-9]|86)(\d{7})$/,
      vinaphone: /^(0|84)(9[14]|12[34579]|88)(\d{7})$/,
      mobifone: /^(0|84)(9[03]|12[01268]|89)(\d{7})$/,
      vietnamobile: /^(0|84)(92|186|188)(\d{7})$/,
      gmobile: /^(0|84)(99|199)(\d{7})$/,
    },
    telco_new_prefix: {
      viettel: /^(0|84|084|\+84)(86|9[6-8]|3[2-9]|16[2-9])\d{7}$/,
      vinaphone: /^(0|84|084|\+84)(88|91|94|8[1-5]|(12([3-5]|7|9)))\d{7}$/,
      mobifone: /^(0|84|084|\+84)(89|90|93|(7(0|[6-9]))|(12([0-2]|6|8)))\d{7}$/,
      vietnamobile: /^(0|84|084|\+84)(92|52|56|58|186|188)\d{7}$/,
      gmobile: /^(0|84|084|\+84)(99|59|199)\d{7}$/,
    }
  }
};

export function isNewPrefix(p) {
  return phoneRegexNewPrefix.test(p);
}

export function convertPrefixToOld(phone_number) {
  let telco = identifyTelco(phone_number);
  let prefixConfig = swap(config.validator.new_prefix_convert[telco]);
  let current_prefix = phone_number.substring(0, 3);
  if (!prefixConfig[current_prefix]) {
    return phone_number;
  }
  return (
    prefixConfig[current_prefix] +
      phone_number.substring(3, phone_number.length)
  );
}

export function getCurrentPrefix(phone_number) {
  if (!phone_number) return '';
  return phoneRegexIllegalPrefix.test(phone_number) ? phone_number.substring(0, 4) : phone_number.substring(0, 3);
}

export function getMappedNewPrefix(phone_number) {
  if (!phone_number || isNewPrefix(phone_number)) return getCurrentPrefix(phone_number);

  const telco = identifyTelco(phone_number);
  const prefixConfig = config.validator.new_prefix_convert[telco] || {};
  const current_prefix = getCurrentPrefix(phone_number);
  return prefixConfig[current_prefix];
}

export function swap(json) {
  var ret = {};
  for (var key in json) {
    ret[json[key]] = key;
  }
  return ret;
}

export function identifyTelco(p) {
  const telco_old_prefix = config.validator.telco;
  const telco_new_prefix = config.validator.telco_new_prefix;
  if (isString(p)) {
    for (let telco in telco_old_prefix) {
      if (telco_old_prefix[telco].test(p)) {
        return telco;
      }
    }
    for (let telco in telco_new_prefix) {
      if (telco_new_prefix[telco].test(p)) {
        return telco;
      }
    }
  }
  return 'unknow';
}

