import React from 'react';
import PropsType from 'prop-types';
import SampleComponent from 'components/SampleComponent';

import styles from './homeContainer.module.css';

class HomeContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={styles.homeContainer}>
        <SampleComponent />
      </div>
    );
  }
}

HomeContainer.propTypes =  {
  userInfo: PropsType.object,
};

export default HomeContainer;