import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import MainLayout from 'components/layouts/MainLayout';
import HomeContainer from './containers/HomeContainer';
import './styles/style.css';


function App() {
  return (
    <Router>
      <Switch>
        <MainLayout >
          <Route path='/' component={HomeContainer} />
        </MainLayout>
      </Switch>
    </Router>
  );
}

export default App;
